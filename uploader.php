<?php
require_once('/config/db_connect.php');
$_fileSize = '4097152';//  размер файла (4 MB);
$_whiteList= array("jpeg","jpg","png", "pdf", "rtf", "txt", "doc", "xdoc");
$_fileDirectory = 'uploads/';

if (isset($_POST['uploadfiles'])) {

    $errors = array();
    $done_files = array();
    $files      = $_FILES; // полученные файлы
    foreach( $files as $file ){

        $file_name = $file['name'];
        $file_size = $file['size'];
        $file_tmp = $file['tmp_name'];
        $file_type = $file['type'];

    }

    $file_ext = strtolower(end(explode('.', $file_name)));

    if (in_array($file_ext, $_whiteList) === false) {
        $errors[] = "Ошибка!!!!  Не тот формат";
    }
    if ($file_size > $_fileSize) {
        $errors[] = 'Ошибка!!!!  Не тот формат Размер файла болше 2 MB';
    }

    if (empty($errors) == true) {
        $file_name = date('YmdHis') . rand(100, 1000) . '.' . $file_ext;
        move_uploaded_file($file_tmp, $_fileDirectory . $file_name);
        $message =  "<h2>Ваш Файл загружен <h2/> ";

        $sql = "INSERT INTO files (extension, filename, filetype, filesize,  date_time) VALUES (?,?,?,?, NOW() )";
        $stmt = $mysqli ->prepare($sql);
        $stmt->bind_param(ssss,$file_ext,$file_name, $file_type, $file_size   );
        $stmt ->execute();
        $stmt->close();
        $insert_id = $mysqli ->insert_id;
    }

}
$data = $message ? array('alert' => $message ) : array('error' => $errors);
die( json_encode( $data ) );