<h1>Hello I am Mr. Uploader :)</h1>

<body style="text-align: center;padding-top: 240px">
<p> HTML + PHP + JQUERY</p> 

    <input type="file" multiple="multiple"  name="file">
    <button type="submit" class="upload_files" name="uploadfiles"> Upload</button>

    <div class="ajax-reply"></div>

</body>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'></script>
<script>
    (function($){

        var files;

        $('input[type=file]').on('change', function(){
            files = this.files;
        });

        $('.upload_files').on( 'click', function( event ){

            event.stopPropagation();
            event.preventDefault();

            if( typeof files == 'undefined' ) return;

            var data = new FormData();
            $.each( files, function( key, value ){
                data.append( key, value );
            });

            data.append( 'uploadfiles', 1 );

            // AJAX запрос
            $.ajax({
                url         : './uploader.php',
                type        : 'POST',
                data        : data,
                cache       : false,
                dataType    : 'json',
                processData : false,
                contentType : false,
                success     : function( respond, status, jqXHR ){

                    // ОК
                    if( typeof respond.error === 'undefined' ){
                        $('.ajax-reply').html( respond.alert );
                    }
                    // error
                    else {
                        $('.ajax-reply').html( respond.error  );
                    }
                },
                // Server error
                error: function( jqXHR, status, errorThrown ){
                    $('.ajax-reply').html( status, jqXHR );
                }

            });

        });

    })(jQuery)
</script>
